'use strict';

var todos = [], todosPerPage = 5, currentPage = 1;

$(document).ready(function () {
    filterShowTodo();

    // Add new line on click btn

    $('#add-btn').on('click', addNewTodo);

    // Add new line on Enter

    $('.new-text').keypress(function(e) {
        var key = e.which;
        if(key == 13) {// the enter key code
            addNewTodo();
        }
    });

    // Completed this line

    $(".app").on('click', 'input:checkbox', function() {

        var indexParentElement = $(this).parent().attr('data-id');
        todos.forEach(function(item, index, arr){
            if (item.id == indexParentElement) {
                item.complete = !item.complete;
            }
        });

        filterShowTodo();
    });

    // Completed all lines

    $('#all-completed-btn').on('click', function() {
        completeAll();
        filterShowTodo();
    });

    // Deleted all completed lines

    $('.app').on('click', '#delete-completed-btn', function() {
        deleteCompleteAll();
        filterShowTodo();
    });

    // Deleted current line

    $('.app').on('click', '.delete-btn', function() {

        var indexElementForDel = $(this).parent().attr('data-id');

        for(var i = 0; i < todos.length; i++) {
            if(i == indexElementForDel) {
                todos.splice(i, 1);
            }
        }
        todos.forEach(function(item, index, arr){
            item.id = index;
        });

        filterShowTodo();
    });

    // Activate edit mode for line

    $(".app").on('dblclick', '.text', todoEdit);

    $('.app').on('keypress', '.edit-text', function(e) {
        var key = e.which;
        if(key == 13) {// the enter key code
            var editableHtml = $(this).val();
            var viewableText = $("<span class='text'>");
            viewableText.html(editableHtml);
            $(this).replaceWith(viewableText);

            var indexElementForEditOnEnter = viewableText.parent().attr('data-id');
            todos.forEach(function(item, index, arr){
                if (item.id == indexElementForEditOnEnter) {
                    item.text = editableHtml;
                }
            });
            filterShowTodo();
        }
    });

    // Control visible lines

    $('.btn-filter').on('click', function () {
        $('.btn-filter').removeClass('active');
        $(this).addClass('active');
        currentPage = 1;
        filterShowTodo();
    });

    $('.wrap-pagination').on('click', 'li', filterForPagination);

});

// functions

// Add new line
function addNewTodo() {
    var newText = $('.new-text').val().trim();
    if (newText.length>0) {

        todos.push({text: newText, complete: false});
        todos.forEach(function(item, index, arr){
            item.id = index;
        });
        filterShowTodo();
        $('.new-text').val('');
    }
}

// Completed all lines
function completeAll() {
    todos.forEach(function(item, index, arr){
        var todo = todos[index];
        todo.complete = true;
    });
    $('input:checkbox').prop('checked', true);
}

// Delete all completed lines
function deleteCompleteAll() {
    var complete = true;

    for(var i = 0; i < todos.length; i++) {
        if(todos[i].complete == complete) {
            todos.splice(i, 1);
            i--;
        }
    }
}

// Filter for visible lines (all/active/completed)
function filterShowTodo() {
    var activeBtn = $('.btn-filter.active').attr('data-btn');

    switch (activeBtn) {
        case 'show-all-btn':
            var todosAllShow = todos;
            pagination(todosAllShow);
            break;
        case 'show-active-btn':
            var todosActiveShow = todos.filter(function(item) {
                return item.complete !== true;
            });
            pagination(todosActiveShow);
            break;
        case 'show-completed-btn':
            var todosCompletedShow = todos.filter(function(item) {
                return item.complete === true;
            });
            pagination(todosCompletedShow);
    }
}

function pagination(tasksOnPages) {
    var numPages = 1, todosOnCurrentPage = [];
    numPages = Math.ceil(tasksOnPages.length / todosPerPage);
    var paginationStart = '<ul id="pagination">';
    var paginationEnd = '</ul>';
    var paginationTemplate = '<li><a href="javascript:void(0);">';
    var paginationTemplateEnd = '</a></li>';
    var paginationResult = '';

    if (numPages == 0) {
        $(".wrap-pagination").html('');
        todosOnCurrentPage = [];
        render(todosOnCurrentPage);
    }

    for (var i = 1; i <= numPages; i++) {

        if (i != currentPage) {
            paginationResult += paginationTemplate  + i + paginationTemplateEnd;
            $(".wrap-pagination").html(paginationStart + paginationResult + paginationEnd);
        } else {
            paginationResult += '<li>' + i + '</li>';
            $(".wrap-pagination").html(paginationStart + paginationResult + paginationEnd);
        }
        $('.wrap-pagination li').eq(currentPage - 1).addClass('active');

        todosOnCurrentPage = tasksOnPages.slice((currentPage-1) * todosPerPage,
                ((currentPage-1)*todosPerPage) + todosPerPage);

        if (!todosOnCurrentPage.length) {
            currentPage = numPages;
            $('.wrap-pagination li').eq(currentPage - 2).addClass('active');
            todosOnCurrentPage = tasksOnPages.slice((currentPage-1) * todosPerPage,
                ((currentPage-1)*todosPerPage) + todosPerPage);
        }
        render(todosOnCurrentPage);
    }
}

function filterForPagination() {
    var pageClicked = $(this).index() + 1;
    currentPage = pageClicked;
    var activePage = $(this);

    filterShowTodo();
}

// Edit mode for line
function todoEdit() {
    var spanHtml = $(this).html();
    var editableText = $("<input type='text' class='edit-text'/>");
    editableText.val(spanHtml);
    $(this).replaceWith(editableText);
    editableText.focus();
    // setup the blur event for this new input
    editableText.blur(editTodoExit); // exit edit mode with blur
}

// Exit edit mode for line
function editTodoExit() {
    var editableHtml = $(this).val();
    var viewableText = $("<span class='text'>");
    viewableText.html(editableHtml);
    $(this).replaceWith(viewableText);

    var indexElementForEdit = viewableText.parent().attr('data-id');
    todos.forEach(function(item, index, arr){
        if (item.id == indexElementForEdit) {
            item.text = editableHtml;
        }
    });
    filterShowTodo();
}

// render list of tasks
function render(tasksFiltered) {

    $( ".tasks" ).html('');
    var numberCompleteTodo = 0, numberActiveTodo = 0;

    tasksFiltered.forEach(function(item, index, arr){
        var completeTodo = item.complete;

        $( ".tasks" ).append( "<div class='line" + (completeTodo? ' completed': '') + "' data-id='" + item.id + "'><input type='checkbox'><span class='text'>" + item.text + "</span><button class='delete-btn'>X</button></div>");

        if (completeTodo) {
            $('.line').eq(index).find('input:checkbox').prop('checked', true);
        }

        console.log(completeTodo);
    });

    todos.forEach(function(item, index, arr){
        if (item.complete) {
            numberCompleteTodo += 1;

        } else {
            numberActiveTodo +=1;
        }
    });

    $('.completed-counter').html(numberCompleteTodo);
    $('.active-counter').html(numberActiveTodo);

    console.log(tasksFiltered);
}